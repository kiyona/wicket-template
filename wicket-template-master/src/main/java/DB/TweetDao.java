package DB;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;


@Dao(config = DBConfig.class)
public interface TweetDao {

    @Insert
    public int insert(Tweet tweet);
    
    @Select
    public Optional<Tweet> selectTweet(LocalDateTime time, int userId);
    
    @Select
    public List<NewTweet> selectAllTweet();
    
    @Select
    public List<NewTweet> selectTweetwithID(List<Integer> id);
    
//    @Select
//    public List<NewTweet> selectKiyonaTweet();
//    
//    @Select
//    public List<NewTweet> selectOtoTweet();
//    
//    @Select
//    public List<NewTweet> select12Tweet();
//    
//    @Select
//    public List<NewTweet> select23Tweet();
//    
//    @Select
//    public List<NewTweet> select31Tweet();
    
    @Update
    public int Update(Tweet tweet);
    
    @Delete
    public int delete(Tweet tweet);
}
