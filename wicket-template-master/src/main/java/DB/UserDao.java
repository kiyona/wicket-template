package DB;


import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

import java.util.List;
import java.util.Optional;

@Dao(config = DBConfig.class)
public interface UserDao {

    @Insert
    public int insert(User user);
    
    @Select
    public Optional<User> selectUser(String name, String password);
    
    @Select
    public String selectUserPass(int userId);
    
    @Select
    public String selectUserName(int userId);
    
    @Select
    public List<User> selectAllUser();
    
    @Update
    public int Update(User user);
    
    @Delete
    public int delete(User user);
}
