package DB;


import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

import java.util.List;
import java.util.Optional;

@Dao(config = DBConfig.class)
public interface FollowDao {

    @Insert
    public int insert(Follow follow);
    
    @Select
    public List<User> selectAllFollowee(User user);
    
    @Delete
    public int delete(Follow follow);
}
