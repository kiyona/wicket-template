package DB;

import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;
import org.seasar.doma.jdbc.entity.NamingType;

@Entity(naming = NamingType.SNAKE_LOWER_CASE)
@Table(name= "follow")
public class Follow {

    @Id
    private int userid;
    @Id
    private int followid;
    
    public int getUserId() {
        return userid;
    }
    public void setUserId(int id) {
        this.userid = id;
    }
    public int getfollowid() {
        return followid;
    }
    public void setFollowid(int id) {
        this.followid = id;
    }
   
    
    
}
