package wicket.template;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.string.Strings;

public class WicketExamplePage extends WebPage
   {
       /**
        * Constructor
        */
       public WicketExamplePage()
       {
           this(null);
       }
   
       /**
        * Construct.
        * 
        * @param model
        */
       public WicketExamplePage(IModel<?> model)
       {
           super(model);
           final String packageName = getClass().getPackage().getName();
           add(new WicketExampleHeader("mainNavigation", Strings.afterLast(packageName, '.'), this));
           explain();
       }
   
              /**
        * Override base method to provide an explanation
        */
       protected void explain()
       {
       }
   }
