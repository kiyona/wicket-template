package wicket.template;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.google.inject.Inject;

public class HomePage extends WebPage {
    private static final long serialVersionUID = 1L;

    @Inject private InjectionService injectionService;

    public HomePage(final PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

       
    }
}
