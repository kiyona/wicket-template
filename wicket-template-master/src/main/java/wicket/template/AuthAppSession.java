package wicket.template;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;
import org.seasar.doma.jdbc.tx.TransactionManager;

import DB.DBConfig;
import DB.User;
import DB.UserDao;
import DB.UserDaoImpl;
import java.util.Optional;

public class AuthAppSession extends AuthenticatedWebSession {
    private static final long serialVersionUID = 1L;
//    private Roles roles;
    
    private static UserDao userDao = new UserDaoImpl();
    
    private User u;
    
    public User getUser(){
        return u;
    }

    // private HashMap<String, String> map;

    public AuthAppSession(Request request) {
        super(request);

    }

    @Override
    public boolean authenticate(String user, String pass) {
        TransactionManager tm = DBConfig.singleton().getTransactionManager();
        
        Optional<User> opt1 =

        tm.required(() -> {
            return userDao.selectUser(user, pass);
        });
        
        if(opt1.isPresent()){
            u = opt1.get();
            return true;
        }
        return false;
    }

    @Override
    public Roles getRoles() {
        
        if (isSignedIn())
        {
            // If the user is signed in, they have these roles
            return new Roles(Roles.ADMIN);
        }
        return null;
    }
}
