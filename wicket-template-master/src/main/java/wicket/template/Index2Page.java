package wicket.template;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.seasar.doma.jdbc.tx.TransactionManager;

import DB.DBConfig;
import DB.FollowDao;
import DB.NewTweet;
import DB.Tweet;
import DB.TweetDao;
import DB.User;

import com.google.inject.Inject;

@AuthorizeInstantiation({ Roles.ADMIN, Roles.USER })
public class Index2Page extends WebPage {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** A global list of all comments from all users across all sessions */

    @Inject private TweetDao tweetDao;
    @Inject private FollowDao followDao;
    private ArrayList<User> follow;
    AuthAppSession session = (AuthAppSession) getSession();

    public Index2Page() {
        super();
    }

    protected void onInitialize() {

        super.onInitialize();
        
        follow = new ArrayList<>();
        
        TransactionManager tmfollow = DBConfig.singleton().getTransactionManager();
        tmfollow.required(() -> {
            follow.addAll(followDao.selectAllFollowee(session.getUser()));
            for(User u:follow){
                System.out.println(u.getName());
                }
        });
        
        add(new Label("nowuser", session.getUser().getName()));

        // Add comment form
        add(new CommentForm("commentForm"));
        // Add commentListView of existing comments
        add(new PropertyListView<NewTweet>("comments", new LoadableDetachableModel<List<NewTweet>>() {

            private static final long serialVersionUID = 1L;
            
            @Override
            protected List<NewTweet> load() {
                // tweetDao.se
                // TODO Auto-generated method stub
                TransactionManager tm = DBConfig.singleton().getTransactionManager();

                List<Integer> userid = new ArrayList<>();
                for(User u: follow){
                    userid.add(u.getUserId());
                }
                                
                return tm.required(() -> {
                    return tweetDao.selectTweetwithID(userid);
                });


            }
        }) {
            /**
                 * 
                 */
            private static final long serialVersionUID = 1L;

            public void populateItem(final ListItem<NewTweet> listItem) {
                listItem.add(new Label("date", new PropertyModel<NewTweet>(listItem.getDefaultModelObject(), "time")));
                listItem.add(new Label("user", new PropertyModel<NewTweet>(listItem.getDefaultModelObject(), "name")));
                listItem.add(new MultiLineLabel("text", new PropertyModel<NewTweet>(listItem.getDefaultModelObject(), "tweet")));
            }
        }).setVersioned(false);
    }

       public final class CommentForm extends Form<ValueMap> {

        private static final long serialVersionUID = 1L;

        public CommentForm(final String id) {
            // Construct form with no validation listener
            super(id, new CompoundPropertyModel<ValueMap>(new ValueMap()));

            // Add text entry widget
            add(new TextArea<String>("text").setType(String.class));

        }

        /**
         * Show the resulting valid edit
         */
        @Override
        public final void onSubmit() {
            ValueMap values = getModelObject();

            TransactionManager tm = DBConfig.singleton().getTransactionManager();
            Tweet tweet = new Tweet();
            tweet.setTime(LocalDateTime.now());
            tweet.setTweet(values.getString("text"));
            AuthAppSession session = (AuthAppSession) getSession();
            tweet.setUserId(session.getUser().getUserId());

            tm.required(() -> {
                tweetDao.insert(tweet);
            });

            // Clear out the text component
            values.put("text", "");
        }

    }

    Link<String> link = new Link<String>("link") {
        private static final long serialVersionUID = 1L;

        @Override
        public void onClick() {
            this.setResponsePage(IndexPage.class);
        }
    };
    {
        this.add(link);
        Link<String> link2 = new Link<String>("link2") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick() {
                this.setResponsePage(AuthSignOutPage.class);
            }
        };
        this.add(link2);
    }
}
