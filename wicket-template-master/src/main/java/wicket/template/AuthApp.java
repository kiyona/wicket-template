package wicket.template;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.authroles.authentication.*;
import org.apache.wicket.guice.GuiceComponentInjector;
import org.apache.wicket.markup.html.WebPage;
//import org.apache.wicket.protocol.http.WebApplication;


/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 *
 * @see wicket.template.Start#main(String[])
 */
public class AuthApp extends AuthenticatedWebApplication {
    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
   
    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        this.getRequestCycleSettings().setResponseRequestEncoding("UTF-8");
        this.getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
        getComponentInstantiationListeners().add(new GuiceComponentInjector(this, new GuiceModule()));
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return AuthSignInPage.class;
    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return AuthAppSession.class;
    }
    
    protected void onUnauthorizedPage(Page page) {
        throw new RestartResponseAtInterceptPageException(ErrorPage.class);
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

//    @Override
//    public String getConfigurationType() {
//        return WebApplication.DEVELOPMENT;
//    }
    
    
}
