package wicket.template;

import java.time.LocalDateTime;


//import org.apache.wicket.IClusterable;


/**
 * Simple "POJO" bean for guestbook comments.
 * 
 * @author Jonathan Locke
 */
public class Comment 
{
    private String text;
    private LocalDateTime date = LocalDateTime.now();
    /**
     * Constructor
     */
    public Comment()
    {
    }

    /**
     * Copy constructor
     * 
     * @param comment
     *            The comment to copy
     */
    public Comment(final Comment comment)
    {
        this.text = comment.text;
        this.date = comment.date;
//        this.userId = comment.userId;
    }

    /**
     * @return Returns the text.
     */
    public String getText()
    {
        return text;
    }

    /**
     * @param text
     *            The text to set.
     */
    public void setText(String text)
    {
        this.text = text;
    }

    /**
     * @return Returns the date.
     */
    public LocalDateTime getDate()
    {
        return date;
    }

    /**
     * @param date
     *            The date to set.
     */
    public void setDate(LocalDateTime date)
    {
        this.date = date;
    }
    

    /**
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return "[Comment date = " + date + ", text = " + text + "]";
    }
}