package wicket.template;

import DB.FollowDao;
import DB.FollowDaoImpl;
import DB.TweetDao;
import DB.TweetDaoImpl;
import DB.UserDao;
import DB.UserDaoImpl;

import com.google.inject.AbstractModule;

public class GuiceModule extends AbstractModule{

    @Override
    protected void configure() {
        bind(UserDao.class).toInstance(new UserDaoImpl());
        bind(TweetDao.class).toInstance(new TweetDaoImpl());
        bind(FollowDao.class).toInstance(new FollowDaoImpl());
    }

    
}
