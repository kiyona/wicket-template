package wicket.template;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormChoiceComponentUpdatingBehavior;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.seasar.doma.jdbc.tx.TransactionManager;

import DB.DBConfig;
import DB.Follow;
import DB.FollowDao;
import DB.NewTweet;
import DB.Tweet;
import DB.TweetDao;
import DB.User;
import DB.UserDao;
import DB.UserDaoImpl;

import com.google.inject.Inject;

@AuthorizeInstantiation({ Roles.ADMIN, Roles.USER })
public class IndexPage extends WebPage {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** A global list of all comments from all users across all sessions */

    private static UserDao userDao = new UserDaoImpl();
    @Inject private TweetDao tweetDao;
    @Inject private FollowDao followDao;

    private static Model<ArrayList<User>> model;
    private CheckBoxMultipleChoice<User> radio;
    private DefineFollow defineFollow = new DefineFollow("check");

    public IndexPage() {
        super();
    }

    protected void onInitialize() {

        super.onInitialize();

        AuthAppSession session = (AuthAppSession) getSession();

        add(new Label("nowuser", session.getUser().getName()));

        add(defineFollow);
        // Add comment form
        add(new CommentForm("commentForm"));
        // Add commentListView of existing comments
        add(new PropertyListView<NewTweet>("comments", new LoadableDetachableModel<List<NewTweet>>() {

            private static final long serialVersionUID = 1L;

            @Override
            protected List<NewTweet> load() {
                // tweetDao.se
                // TODO Auto-generated method stub
                TransactionManager tm = DBConfig.singleton().getTransactionManager();

                AuthAppSession session = (AuthAppSession) getSession();
                int userid = session.getUser().getUserId();
                List<Integer> user = new ArrayList<>();
                user.add(userid);

                return tm.required(() -> {
                    return tweetDao.selectTweetwithID(user);
                });

            }
        }) {
            /**
                 * 
                 */
            private static final long serialVersionUID = 1L;

            public void populateItem(final ListItem<NewTweet> listItem) {
                listItem.add(new Label("date", new PropertyModel<NewTweet>(listItem.getDefaultModelObject(), "time")));
                listItem.add(new Label("user", new PropertyModel<NewTweet>(listItem.getDefaultModelObject(), "name")));
                listItem.add(new MultiLineLabel("text", new PropertyModel<NewTweet>(listItem.getDefaultModelObject(), "tweet")));
            }
        }).setVersioned(false);

            }

    private void add(DefineFollow defineFollow) {
        // TODO Auto-generated method stub

    }

    public final class CommentForm extends Form<ValueMap> {

        private static final long serialVersionUID = 1L;

        public CommentForm(final String id) {
            // Construct form with no validation listener
            super(id, new CompoundPropertyModel<ValueMap>(new ValueMap()));

            // Add text entry widget
            add(new TextArea<String>("text").setType(String.class));

            // // Add simple automated spam prevention measure.
            // add(new TextField<String>("comment").setType(String.class));
        }

        /**
         * Show the resulting valid edit
         */
        @Override
        public final void onSubmit() {
            ValueMap values = getModelObject();

            TransactionManager tm = DBConfig.singleton().getTransactionManager();
            Tweet tweet = new Tweet();
            tweet.setTime(LocalDateTime.now());
            tweet.setTweet(values.getString("text"));
            System.out.println(values);
            AuthAppSession session = (AuthAppSession) getSession();
            tweet.setUserId(session.getUser().getUserId());

            tm.required(() -> {
                tweetDao.insert(tweet);
            });

            // Clear out the text component
            values.put("text", "");
        }

    }

    public final class DefineFollow {
        List<User> userlist;

        public DefineFollow(final String id) {
            TransactionManager tm = DBConfig.singleton().getTransactionManager();
            tm.required(() -> {
                userlist = userDao.selectAllUser();
            });

            AuthAppSession session = (AuthAppSession) getSession();
            model = new Model<ArrayList<User>>();

            ArrayList<User> selectedList = new ArrayList<>();

            TransactionManager tmfollow = DBConfig.singleton().getTransactionManager();
            tmfollow.required(() -> {
                selectedList.addAll(followDao.selectAllFollowee(session.getUser()));
                for (User u : selectedList) {
                    System.out.println(u.getName());
                }
            });

            selectedList.add(session.getUser());
            model.setObject(selectedList);
            radio = new CheckBoxMultipleChoice<User>(id, model, userlist, new IChoiceRenderer<User>() {

                private static final long serialVersionUID = 1L;

                @Override
                public Object getDisplayValue(User object) {
                    // TODO Auto-generated method stub
                    return object.getName();
                }

                @Override
                public String getIdValue(User object, int index) {
                    // TODO Auto-generated method stub
                    return String.valueOf(object.getUserId());
                }

                @Override
                public User getObject(String id, IModel<? extends List<? extends User>> choices) {
                    // TODO Auto-generated method stub
                    return choices.getObject().get(0);
                }

            });

            radio.add(new AjaxFormChoiceComponentUpdatingBehavior() {

                /**
                 * 
                 */
                private static final long serialVersionUID = 1L;

                @Override
                protected void onUpdate(AjaxRequestTarget target) {
                    // TODO Auto-generated method stub
                    getComponent().getDefaultModelObjectAsString();
                }
            });

            add(radio);

        }
    }

    public void insertFollow() {
        Follow follow = new Follow();
        ArrayList<Integer> followID = new ArrayList<>();
        ArrayList<Integer> modelID = new ArrayList<>();
        AuthAppSession session = (AuthAppSession) getSession();
        follow.setUserId(session.getUser().getUserId());
        
        for(User user:model.getObject()){
            modelID.add(user.getUserId());
        }
        
        TransactionManager tm01 = DBConfig.singleton().getTransactionManager();
        tm01.required(() -> {
            for(User user:followDao.selectAllFollowee(session.getUser())){
                followID.add(user.getUserId());
            }
            
        });
        
        
        for (User u : model.getObject()) {
            System.out.println(u.getName());
            TransactionManager tm = DBConfig.singleton().getTransactionManager();
            tm.required(() -> {
                if (followID.contains(u.getUserId())) {
                }else{
                    follow.setFollowid(u.getUserId());
                    followDao.insert(follow);
                }
            });
        }
        
        for (Integer i : followID) {
            TransactionManager tm = DBConfig.singleton().getTransactionManager();
            tm.required(() -> {
                if (modelID.contains(i)) {
                }else{
                    follow.setFollowid(i);
                    followDao.delete(follow);
                }
            });
        }
        
    }
    
    Link<String> link = new Link<String>("link") {
        private static final long serialVersionUID = 1L;

        @Override
        public void onClick() {
            // radio.updateModel();
            insertFollow();
            this.setResponsePage(new Index2Page());
        }
    };

    {
        this.add(link);
        Link<String> link2 = new Link<String>("link2") {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClick() {
                this.setResponsePage(AuthSignOutPage.class);
            }
        };
        this.add(link2);
    }
}
