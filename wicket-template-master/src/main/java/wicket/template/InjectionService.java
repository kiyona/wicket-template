package wicket.template;

import com.google.inject.Singleton;

@Singleton
public class InjectionService {

    public String getMessage() {
        return "Injected by Guice";
    }
}
