package DB;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-07-29T17:09:31.974+0900")
public final class _NewTweet extends org.seasar.doma.jdbc.entity.AbstractEntityType<DB.NewTweet> {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final _NewTweet __singleton = new _NewTweet();

    private final org.seasar.doma.jdbc.entity.NamingType __namingType = org.seasar.doma.jdbc.entity.NamingType.SNAKE_LOWER_CASE;

    /** the name */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, DB.NewTweet, java.lang.String, Object> $name = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(DB.NewTweet.class, java.lang.String.class, java.lang.String.class, () -> new org.seasar.doma.wrapper.StringWrapper(), null, null, "name", "", __namingType, true, true, false);

    /** the time */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, DB.NewTweet, java.time.LocalDateTime, Object> $time = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(DB.NewTweet.class, java.time.LocalDateTime.class, java.time.LocalDateTime.class, () -> new org.seasar.doma.wrapper.LocalDateTimeWrapper(), null, null, "time", "", __namingType, true, true, false);

    /** the tweet */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, DB.NewTweet, java.lang.String, Object> $tweet = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(DB.NewTweet.class, java.lang.String.class, java.lang.String.class, () -> new org.seasar.doma.wrapper.StringWrapper(), null, null, "tweet", "", __namingType, true, true, false);

    private final java.util.function.Supplier<org.seasar.doma.jdbc.entity.NullEntityListener<DB.NewTweet>> __listenerSupplier;

    private final boolean __immutable;

    private final String __catalogName;

    private final String __schemaName;

    private final String __tableName;

    private final boolean __isQuoteRequired;

    private final String __name;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> __idPropertyTypes;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> __entityPropertyTypes;

    private final java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> __entityPropertyTypeMap;

    private _NewTweet() {
        __listenerSupplier = () -> ListenerHolder.listener;
        __immutable = false;
        __name = "NewTweet";
        __catalogName = "";
        __schemaName = "";
        __tableName = "tweet";
        __isQuoteRequired = false;
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> __idList = new java.util.ArrayList<>();
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> __list = new java.util.ArrayList<>(3);
        java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> __map = new java.util.HashMap<>(3);
        __list.add($name);
        __map.put("name", $name);
        __list.add($time);
        __map.put("time", $time);
        __list.add($tweet);
        __map.put("tweet", $tweet);
        __idPropertyTypes = java.util.Collections.unmodifiableList(__idList);
        __entityPropertyTypes = java.util.Collections.unmodifiableList(__list);
        __entityPropertyTypeMap = java.util.Collections.unmodifiableMap(__map);
    }

    @Override
    public org.seasar.doma.jdbc.entity.NamingType getNamingType() {
        return __namingType;
    }

    @Override
    public boolean isImmutable() {
        return __immutable;
    }

    @Override
    public String getName() {
        return __name;
    }

    @Override
    public String getCatalogName() {
        return __catalogName;
    }

    @Override
    public String getSchemaName() {
        return __schemaName;
    }

    @Override
    public String getTableName() {
        return getTableName(org.seasar.doma.jdbc.Naming.DEFAULT::apply);
    }

    @Override
    public String getTableName(java.util.function.BiFunction<org.seasar.doma.jdbc.entity.NamingType, String, String> namingFunction) {
        if (__tableName.isEmpty()) {
            return namingFunction.apply(__namingType, __name);
        }
        return __tableName;
    }

    @Override
    public boolean isQuoteRequired() {
        return __isQuoteRequired;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preInsert(DB.NewTweet entity, org.seasar.doma.jdbc.entity.PreInsertContext<DB.NewTweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preUpdate(DB.NewTweet entity, org.seasar.doma.jdbc.entity.PreUpdateContext<DB.NewTweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preDelete(DB.NewTweet entity, org.seasar.doma.jdbc.entity.PreDeleteContext<DB.NewTweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preDelete(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postInsert(DB.NewTweet entity, org.seasar.doma.jdbc.entity.PostInsertContext<DB.NewTweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postUpdate(DB.NewTweet entity, org.seasar.doma.jdbc.entity.PostUpdateContext<DB.NewTweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postDelete(DB.NewTweet entity, org.seasar.doma.jdbc.entity.PostDeleteContext<DB.NewTweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postDelete(entity, context);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> getEntityPropertyTypes() {
        return __entityPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?> getEntityPropertyType(String __name) {
        return __entityPropertyTypeMap.get(__name);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.NewTweet, ?>> getIdPropertyTypes() {
        return __idPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.GeneratedIdPropertyType<java.lang.Object, DB.NewTweet, ?, ?> getGeneratedIdPropertyType() {
        return null;
    }

    @Override
    public org.seasar.doma.jdbc.entity.VersionPropertyType<java.lang.Object, DB.NewTweet, ?, ?> getVersionPropertyType() {
        return null;
    }

    @Override
    public DB.NewTweet newEntity(java.util.Map<String, org.seasar.doma.jdbc.entity.Property<DB.NewTweet, ?>> __args) {
        DB.NewTweet entity = new DB.NewTweet();
        __args.values().forEach(v -> v.save(entity));
        return entity;
    }

    @Override
    public Class<DB.NewTweet> getEntityClass() {
        return DB.NewTweet.class;
    }

    @Override
    public DB.NewTweet getOriginalStates(DB.NewTweet __entity) {
        return null;
    }

    @Override
    public void saveCurrentStates(DB.NewTweet __entity) {
    }

    /**
     * @return the singleton
     */
    public static _NewTweet getSingletonInternal() {
        return __singleton;
    }

    /**
     * @return the new instance
     */
    public static _NewTweet newInstance() {
        return new _NewTweet();
    }

    private static class ListenerHolder {
        private static org.seasar.doma.jdbc.entity.NullEntityListener<DB.NewTweet> listener = new org.seasar.doma.jdbc.entity.NullEntityListener<>();
    }

}
