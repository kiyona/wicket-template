package DB;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-07-29T17:47:24.738+0900")
public class FollowDaoImpl extends org.seasar.doma.internal.jdbc.dao.AbstractDao implements DB.FollowDao {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final java.lang.reflect.Method __method0 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.FollowDao.class, "delete", DB.Follow.class);

    private static final java.lang.reflect.Method __method1 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.FollowDao.class, "insert", DB.Follow.class);

    private static final java.lang.reflect.Method __method2 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.FollowDao.class, "selectAllFollowee", DB.User.class);

    /** */
    public FollowDaoImpl() {
        super(DB.DBConfig.singleton());
    }

    /**
     * @param connection the connection
     */
    public FollowDaoImpl(java.sql.Connection connection) {
        super(DB.DBConfig.singleton(), connection);
    }

    /**
     * @param dataSource the dataSource
     */
    public FollowDaoImpl(javax.sql.DataSource dataSource) {
        super(DB.DBConfig.singleton(), dataSource);
    }

    /**
     * @param config the configuration
     */
    protected FollowDaoImpl(org.seasar.doma.jdbc.Config config) {
        super(config);
    }

    /**
     * @param config the configuration
     * @param connection the connection
     */
    protected FollowDaoImpl(org.seasar.doma.jdbc.Config config, java.sql.Connection connection) {
        super(config, connection);
    }

    /**
     * @param config the configuration
     * @param dataSource the dataSource
     */
    protected FollowDaoImpl(org.seasar.doma.jdbc.Config config, javax.sql.DataSource dataSource) {
        super(config, dataSource);
    }

    @Override
    public int delete(DB.Follow follow) {
        entering("DB.FollowDaoImpl", "delete", follow);
        try {
            if (follow == null) {
                throw new org.seasar.doma.DomaNullPointerException("follow");
            }
            org.seasar.doma.jdbc.query.AutoDeleteQuery<DB.Follow> __query = getQueryImplementors().createAutoDeleteQuery(__method0, DB._Follow.getSingletonInternal());
            __query.setMethod(__method0);
            __query.setConfig(__config);
            __query.setEntity(follow);
            __query.setCallerClassName("DB.FollowDaoImpl");
            __query.setCallerMethodName("delete");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setVersionIgnored(false);
            __query.setOptimisticLockExceptionSuppressed(false);
            __query.prepare();
            org.seasar.doma.jdbc.command.DeleteCommand __command = getCommandImplementors().createDeleteCommand(__method0, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.FollowDaoImpl", "delete", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.FollowDaoImpl", "delete", __e);
            throw __e;
        }
    }

    @Override
    public int insert(DB.Follow follow) {
        entering("DB.FollowDaoImpl", "insert", follow);
        try {
            if (follow == null) {
                throw new org.seasar.doma.DomaNullPointerException("follow");
            }
            org.seasar.doma.jdbc.query.AutoInsertQuery<DB.Follow> __query = getQueryImplementors().createAutoInsertQuery(__method1, DB._Follow.getSingletonInternal());
            __query.setMethod(__method1);
            __query.setConfig(__config);
            __query.setEntity(follow);
            __query.setCallerClassName("DB.FollowDaoImpl");
            __query.setCallerMethodName("insert");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.prepare();
            org.seasar.doma.jdbc.command.InsertCommand __command = getCommandImplementors().createInsertCommand(__method1, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.FollowDaoImpl", "insert", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.FollowDaoImpl", "insert", __e);
            throw __e;
        }
    }

    @Override
    public java.util.List<DB.User> selectAllFollowee(DB.User user) {
        entering("DB.FollowDaoImpl", "selectAllFollowee", user);
        try {
            if (user == null) {
                throw new org.seasar.doma.DomaNullPointerException("user");
            }
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method2);
            __query.setMethod(__method2);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/FollowDao/selectAllFollowee.sql");
            __query.setEntityType(DB._User.getSingletonInternal());
            __query.addParameter("user", DB.User.class, user);
            __query.setCallerClassName("DB.FollowDaoImpl");
            __query.setCallerMethodName("selectAllFollowee");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.List<DB.User>> __command = getCommandImplementors().createSelectCommand(__method2, __query, new org.seasar.doma.internal.jdbc.command.EntityResultListHandler<DB.User>(DB._User.getSingletonInternal()));
            java.util.List<DB.User> __result = __command.execute();
            __query.complete();
            exiting("DB.FollowDaoImpl", "selectAllFollowee", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.FollowDaoImpl", "selectAllFollowee", __e);
            throw __e;
        }
    }

}
