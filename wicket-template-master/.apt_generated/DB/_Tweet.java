package DB;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-07-29T17:09:32.167+0900")
public final class _Tweet extends org.seasar.doma.jdbc.entity.AbstractEntityType<DB.Tweet> {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final _Tweet __singleton = new _Tweet();

    private final org.seasar.doma.jdbc.entity.NamingType __namingType = org.seasar.doma.jdbc.entity.NamingType.SNAKE_LOWER_CASE;

    /** the time */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, DB.Tweet, java.time.LocalDateTime, Object> $time = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(DB.Tweet.class, java.time.LocalDateTime.class, java.time.LocalDateTime.class, () -> new org.seasar.doma.wrapper.LocalDateTimeWrapper(), null, null, "time", "", __namingType, true, true, false);

    /** the tweet */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, DB.Tweet, java.lang.String, Object> $tweet = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(DB.Tweet.class, java.lang.String.class, java.lang.String.class, () -> new org.seasar.doma.wrapper.StringWrapper(), null, null, "tweet", "", __namingType, true, true, false);

    /** the tweetId */
    public final org.seasar.doma.jdbc.entity.AssignedIdPropertyType<java.lang.Object, DB.Tweet, java.lang.Integer, Object> $tweetId = new org.seasar.doma.jdbc.entity.AssignedIdPropertyType<>(DB.Tweet.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "tweetId", "", __namingType, false);

    /** the userId */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, DB.Tweet, java.lang.Integer, Object> $userId = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(DB.Tweet.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "userId", "", __namingType, true, true, false);

    private final java.util.function.Supplier<org.seasar.doma.jdbc.entity.NullEntityListener<DB.Tweet>> __listenerSupplier;

    private final boolean __immutable;

    private final String __catalogName;

    private final String __schemaName;

    private final String __tableName;

    private final boolean __isQuoteRequired;

    private final String __name;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> __idPropertyTypes;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> __entityPropertyTypes;

    private final java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> __entityPropertyTypeMap;

    private _Tweet() {
        __listenerSupplier = () -> ListenerHolder.listener;
        __immutable = false;
        __name = "Tweet";
        __catalogName = "";
        __schemaName = "";
        __tableName = "tweet";
        __isQuoteRequired = false;
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> __idList = new java.util.ArrayList<>();
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> __list = new java.util.ArrayList<>(4);
        java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> __map = new java.util.HashMap<>(4);
        __list.add($time);
        __map.put("time", $time);
        __list.add($tweet);
        __map.put("tweet", $tweet);
        __idList.add($tweetId);
        __list.add($tweetId);
        __map.put("tweetId", $tweetId);
        __list.add($userId);
        __map.put("userId", $userId);
        __idPropertyTypes = java.util.Collections.unmodifiableList(__idList);
        __entityPropertyTypes = java.util.Collections.unmodifiableList(__list);
        __entityPropertyTypeMap = java.util.Collections.unmodifiableMap(__map);
    }

    @Override
    public org.seasar.doma.jdbc.entity.NamingType getNamingType() {
        return __namingType;
    }

    @Override
    public boolean isImmutable() {
        return __immutable;
    }

    @Override
    public String getName() {
        return __name;
    }

    @Override
    public String getCatalogName() {
        return __catalogName;
    }

    @Override
    public String getSchemaName() {
        return __schemaName;
    }

    @Override
    public String getTableName() {
        return getTableName(org.seasar.doma.jdbc.Naming.DEFAULT::apply);
    }

    @Override
    public String getTableName(java.util.function.BiFunction<org.seasar.doma.jdbc.entity.NamingType, String, String> namingFunction) {
        if (__tableName.isEmpty()) {
            return namingFunction.apply(__namingType, __name);
        }
        return __tableName;
    }

    @Override
    public boolean isQuoteRequired() {
        return __isQuoteRequired;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preInsert(DB.Tweet entity, org.seasar.doma.jdbc.entity.PreInsertContext<DB.Tweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preUpdate(DB.Tweet entity, org.seasar.doma.jdbc.entity.PreUpdateContext<DB.Tweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preDelete(DB.Tweet entity, org.seasar.doma.jdbc.entity.PreDeleteContext<DB.Tweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preDelete(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postInsert(DB.Tweet entity, org.seasar.doma.jdbc.entity.PostInsertContext<DB.Tweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postUpdate(DB.Tweet entity, org.seasar.doma.jdbc.entity.PostUpdateContext<DB.Tweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postDelete(DB.Tweet entity, org.seasar.doma.jdbc.entity.PostDeleteContext<DB.Tweet> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postDelete(entity, context);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> getEntityPropertyTypes() {
        return __entityPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?> getEntityPropertyType(String __name) {
        return __entityPropertyTypeMap.get(__name);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Tweet, ?>> getIdPropertyTypes() {
        return __idPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.GeneratedIdPropertyType<java.lang.Object, DB.Tweet, ?, ?> getGeneratedIdPropertyType() {
        return null;
    }

    @Override
    public org.seasar.doma.jdbc.entity.VersionPropertyType<java.lang.Object, DB.Tweet, ?, ?> getVersionPropertyType() {
        return null;
    }

    @Override
    public DB.Tweet newEntity(java.util.Map<String, org.seasar.doma.jdbc.entity.Property<DB.Tweet, ?>> __args) {
        DB.Tweet entity = new DB.Tweet();
        __args.values().forEach(v -> v.save(entity));
        return entity;
    }

    @Override
    public Class<DB.Tweet> getEntityClass() {
        return DB.Tweet.class;
    }

    @Override
    public DB.Tweet getOriginalStates(DB.Tweet __entity) {
        return null;
    }

    @Override
    public void saveCurrentStates(DB.Tweet __entity) {
    }

    /**
     * @return the singleton
     */
    public static _Tweet getSingletonInternal() {
        return __singleton;
    }

    /**
     * @return the new instance
     */
    public static _Tweet newInstance() {
        return new _Tweet();
    }

    private static class ListenerHolder {
        private static org.seasar.doma.jdbc.entity.NullEntityListener<DB.Tweet> listener = new org.seasar.doma.jdbc.entity.NullEntityListener<>();
    }

}
