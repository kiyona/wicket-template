package DB;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-07-29T17:09:34.316+0900")
public class TweetDaoImpl extends org.seasar.doma.internal.jdbc.dao.AbstractDao implements DB.TweetDao {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final java.lang.reflect.Method __method0 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.TweetDao.class, "Update", DB.Tweet.class);

    private static final java.lang.reflect.Method __method1 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.TweetDao.class, "delete", DB.Tweet.class);

    private static final java.lang.reflect.Method __method2 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.TweetDao.class, "insert", DB.Tweet.class);

    private static final java.lang.reflect.Method __method3 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.TweetDao.class, "selectAllTweet");

    private static final java.lang.reflect.Method __method4 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.TweetDao.class, "selectTweet", java.time.LocalDateTime.class, int.class);

    private static final java.lang.reflect.Method __method5 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.TweetDao.class, "selectTweetwithID", java.util.List.class);

    /** */
    public TweetDaoImpl() {
        super(DB.DBConfig.singleton());
    }

    /**
     * @param connection the connection
     */
    public TweetDaoImpl(java.sql.Connection connection) {
        super(DB.DBConfig.singleton(), connection);
    }

    /**
     * @param dataSource the dataSource
     */
    public TweetDaoImpl(javax.sql.DataSource dataSource) {
        super(DB.DBConfig.singleton(), dataSource);
    }

    /**
     * @param config the configuration
     */
    protected TweetDaoImpl(org.seasar.doma.jdbc.Config config) {
        super(config);
    }

    /**
     * @param config the configuration
     * @param connection the connection
     */
    protected TweetDaoImpl(org.seasar.doma.jdbc.Config config, java.sql.Connection connection) {
        super(config, connection);
    }

    /**
     * @param config the configuration
     * @param dataSource the dataSource
     */
    protected TweetDaoImpl(org.seasar.doma.jdbc.Config config, javax.sql.DataSource dataSource) {
        super(config, dataSource);
    }

    @Override
    public int Update(DB.Tweet tweet) {
        entering("DB.TweetDaoImpl", "Update", tweet);
        try {
            if (tweet == null) {
                throw new org.seasar.doma.DomaNullPointerException("tweet");
            }
            org.seasar.doma.jdbc.query.AutoUpdateQuery<DB.Tweet> __query = getQueryImplementors().createAutoUpdateQuery(__method0, DB._Tweet.getSingletonInternal());
            __query.setMethod(__method0);
            __query.setConfig(__config);
            __query.setEntity(tweet);
            __query.setCallerClassName("DB.TweetDaoImpl");
            __query.setCallerMethodName("Update");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setVersionIgnored(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.setUnchangedPropertyIncluded(false);
            __query.setOptimisticLockExceptionSuppressed(false);
            __query.prepare();
            org.seasar.doma.jdbc.command.UpdateCommand __command = getCommandImplementors().createUpdateCommand(__method0, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.TweetDaoImpl", "Update", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.TweetDaoImpl", "Update", __e);
            throw __e;
        }
    }

    @Override
    public int delete(DB.Tweet tweet) {
        entering("DB.TweetDaoImpl", "delete", tweet);
        try {
            if (tweet == null) {
                throw new org.seasar.doma.DomaNullPointerException("tweet");
            }
            org.seasar.doma.jdbc.query.AutoDeleteQuery<DB.Tweet> __query = getQueryImplementors().createAutoDeleteQuery(__method1, DB._Tweet.getSingletonInternal());
            __query.setMethod(__method1);
            __query.setConfig(__config);
            __query.setEntity(tweet);
            __query.setCallerClassName("DB.TweetDaoImpl");
            __query.setCallerMethodName("delete");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setVersionIgnored(false);
            __query.setOptimisticLockExceptionSuppressed(false);
            __query.prepare();
            org.seasar.doma.jdbc.command.DeleteCommand __command = getCommandImplementors().createDeleteCommand(__method1, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.TweetDaoImpl", "delete", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.TweetDaoImpl", "delete", __e);
            throw __e;
        }
    }

    @Override
    public int insert(DB.Tweet tweet) {
        entering("DB.TweetDaoImpl", "insert", tweet);
        try {
            if (tweet == null) {
                throw new org.seasar.doma.DomaNullPointerException("tweet");
            }
            org.seasar.doma.jdbc.query.AutoInsertQuery<DB.Tweet> __query = getQueryImplementors().createAutoInsertQuery(__method2, DB._Tweet.getSingletonInternal());
            __query.setMethod(__method2);
            __query.setConfig(__config);
            __query.setEntity(tweet);
            __query.setCallerClassName("DB.TweetDaoImpl");
            __query.setCallerMethodName("insert");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.prepare();
            org.seasar.doma.jdbc.command.InsertCommand __command = getCommandImplementors().createInsertCommand(__method2, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.TweetDaoImpl", "insert", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.TweetDaoImpl", "insert", __e);
            throw __e;
        }
    }

    @Override
    public java.util.List<DB.NewTweet> selectAllTweet() {
        entering("DB.TweetDaoImpl", "selectAllTweet");
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method3);
            __query.setMethod(__method3);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/TweetDao/selectAllTweet.sql");
            __query.setEntityType(DB._NewTweet.getSingletonInternal());
            __query.setCallerClassName("DB.TweetDaoImpl");
            __query.setCallerMethodName("selectAllTweet");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.List<DB.NewTweet>> __command = getCommandImplementors().createSelectCommand(__method3, __query, new org.seasar.doma.internal.jdbc.command.EntityResultListHandler<DB.NewTweet>(DB._NewTweet.getSingletonInternal()));
            java.util.List<DB.NewTweet> __result = __command.execute();
            __query.complete();
            exiting("DB.TweetDaoImpl", "selectAllTweet", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.TweetDaoImpl", "selectAllTweet", __e);
            throw __e;
        }
    }

    @Override
    public java.util.Optional<DB.Tweet> selectTweet(java.time.LocalDateTime time, int userId) {
        entering("DB.TweetDaoImpl", "selectTweet", time, userId);
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method4);
            __query.setMethod(__method4);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/TweetDao/selectTweet.sql");
            __query.setEntityType(DB._Tweet.getSingletonInternal());
            __query.addParameter("time", java.time.LocalDateTime.class, time);
            __query.addParameter("userId", int.class, userId);
            __query.setCallerClassName("DB.TweetDaoImpl");
            __query.setCallerMethodName("selectTweet");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.Optional<DB.Tweet>> __command = getCommandImplementors().createSelectCommand(__method4, __query, new org.seasar.doma.internal.jdbc.command.OptionalEntitySingleResultHandler<DB.Tweet>(DB._Tweet.getSingletonInternal()));
            java.util.Optional<DB.Tweet> __result = __command.execute();
            __query.complete();
            exiting("DB.TweetDaoImpl", "selectTweet", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.TweetDaoImpl", "selectTweet", __e);
            throw __e;
        }
    }

    @Override
    public java.util.List<DB.NewTweet> selectTweetwithID(java.util.List<java.lang.Integer> id) {
        entering("DB.TweetDaoImpl", "selectTweetwithID", id);
        try {
            if (id == null) {
                throw new org.seasar.doma.DomaNullPointerException("id");
            }
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method5);
            __query.setMethod(__method5);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/TweetDao/selectTweetwithID.sql");
            __query.setEntityType(DB._NewTweet.getSingletonInternal());
            __query.addParameter("id", java.util.List.class, id);
            __query.setCallerClassName("DB.TweetDaoImpl");
            __query.setCallerMethodName("selectTweetwithID");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.List<DB.NewTweet>> __command = getCommandImplementors().createSelectCommand(__method5, __query, new org.seasar.doma.internal.jdbc.command.EntityResultListHandler<DB.NewTweet>(DB._NewTweet.getSingletonInternal()));
            java.util.List<DB.NewTweet> __result = __command.execute();
            __query.complete();
            exiting("DB.TweetDaoImpl", "selectTweetwithID", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.TweetDaoImpl", "selectTweetwithID", __e);
            throw __e;
        }
    }

}
