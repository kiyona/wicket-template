package DB;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-07-29T17:09:33.375+0900")
public class UserDaoImpl extends org.seasar.doma.internal.jdbc.dao.AbstractDao implements DB.UserDao {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final java.lang.reflect.Method __method0 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "Update", DB.User.class);

    private static final java.lang.reflect.Method __method1 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "delete", DB.User.class);

    private static final java.lang.reflect.Method __method2 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "insert", DB.User.class);

    private static final java.lang.reflect.Method __method3 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "selectAllUser");

    private static final java.lang.reflect.Method __method4 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "selectUser", java.lang.String.class, java.lang.String.class);

    private static final java.lang.reflect.Method __method5 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "selectUserName", int.class);

    private static final java.lang.reflect.Method __method6 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(DB.UserDao.class, "selectUserPass", int.class);

    /** */
    public UserDaoImpl() {
        super(DB.DBConfig.singleton());
    }

    /**
     * @param connection the connection
     */
    public UserDaoImpl(java.sql.Connection connection) {
        super(DB.DBConfig.singleton(), connection);
    }

    /**
     * @param dataSource the dataSource
     */
    public UserDaoImpl(javax.sql.DataSource dataSource) {
        super(DB.DBConfig.singleton(), dataSource);
    }

    /**
     * @param config the configuration
     */
    protected UserDaoImpl(org.seasar.doma.jdbc.Config config) {
        super(config);
    }

    /**
     * @param config the configuration
     * @param connection the connection
     */
    protected UserDaoImpl(org.seasar.doma.jdbc.Config config, java.sql.Connection connection) {
        super(config, connection);
    }

    /**
     * @param config the configuration
     * @param dataSource the dataSource
     */
    protected UserDaoImpl(org.seasar.doma.jdbc.Config config, javax.sql.DataSource dataSource) {
        super(config, dataSource);
    }

    @Override
    public int Update(DB.User user) {
        entering("DB.UserDaoImpl", "Update", user);
        try {
            if (user == null) {
                throw new org.seasar.doma.DomaNullPointerException("user");
            }
            org.seasar.doma.jdbc.query.AutoUpdateQuery<DB.User> __query = getQueryImplementors().createAutoUpdateQuery(__method0, DB._User.getSingletonInternal());
            __query.setMethod(__method0);
            __query.setConfig(__config);
            __query.setEntity(user);
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("Update");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setVersionIgnored(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.setUnchangedPropertyIncluded(false);
            __query.setOptimisticLockExceptionSuppressed(false);
            __query.prepare();
            org.seasar.doma.jdbc.command.UpdateCommand __command = getCommandImplementors().createUpdateCommand(__method0, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "Update", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "Update", __e);
            throw __e;
        }
    }

    @Override
    public int delete(DB.User user) {
        entering("DB.UserDaoImpl", "delete", user);
        try {
            if (user == null) {
                throw new org.seasar.doma.DomaNullPointerException("user");
            }
            org.seasar.doma.jdbc.query.AutoDeleteQuery<DB.User> __query = getQueryImplementors().createAutoDeleteQuery(__method1, DB._User.getSingletonInternal());
            __query.setMethod(__method1);
            __query.setConfig(__config);
            __query.setEntity(user);
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("delete");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setVersionIgnored(false);
            __query.setOptimisticLockExceptionSuppressed(false);
            __query.prepare();
            org.seasar.doma.jdbc.command.DeleteCommand __command = getCommandImplementors().createDeleteCommand(__method1, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "delete", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "delete", __e);
            throw __e;
        }
    }

    @Override
    public int insert(DB.User user) {
        entering("DB.UserDaoImpl", "insert", user);
        try {
            if (user == null) {
                throw new org.seasar.doma.DomaNullPointerException("user");
            }
            org.seasar.doma.jdbc.query.AutoInsertQuery<DB.User> __query = getQueryImplementors().createAutoInsertQuery(__method2, DB._User.getSingletonInternal());
            __query.setMethod(__method2);
            __query.setConfig(__config);
            __query.setEntity(user);
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("insert");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.prepare();
            org.seasar.doma.jdbc.command.InsertCommand __command = getCommandImplementors().createInsertCommand(__method2, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "insert", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "insert", __e);
            throw __e;
        }
    }

    @Override
    public java.util.List<DB.User> selectAllUser() {
        entering("DB.UserDaoImpl", "selectAllUser");
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method3);
            __query.setMethod(__method3);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/UserDao/selectAllUser.sql");
            __query.setEntityType(DB._User.getSingletonInternal());
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("selectAllUser");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.List<DB.User>> __command = getCommandImplementors().createSelectCommand(__method3, __query, new org.seasar.doma.internal.jdbc.command.EntityResultListHandler<DB.User>(DB._User.getSingletonInternal()));
            java.util.List<DB.User> __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "selectAllUser", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "selectAllUser", __e);
            throw __e;
        }
    }

    @Override
    public java.util.Optional<DB.User> selectUser(java.lang.String name, java.lang.String password) {
        entering("DB.UserDaoImpl", "selectUser", name, password);
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method4);
            __query.setMethod(__method4);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/UserDao/selectUser.sql");
            __query.setEntityType(DB._User.getSingletonInternal());
            __query.addParameter("name", java.lang.String.class, name);
            __query.addParameter("password", java.lang.String.class, password);
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("selectUser");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.Optional<DB.User>> __command = getCommandImplementors().createSelectCommand(__method4, __query, new org.seasar.doma.internal.jdbc.command.OptionalEntitySingleResultHandler<DB.User>(DB._User.getSingletonInternal()));
            java.util.Optional<DB.User> __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "selectUser", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "selectUser", __e);
            throw __e;
        }
    }

    @Override
    public java.lang.String selectUserName(int userId) {
        entering("DB.UserDaoImpl", "selectUserName", userId);
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method5);
            __query.setMethod(__method5);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/UserDao/selectUserName.sql");
            __query.addParameter("userId", int.class, userId);
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("selectUserName");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.lang.String> __command = getCommandImplementors().createSelectCommand(__method5, __query, new org.seasar.doma.internal.jdbc.command.BasicSingleResultHandler<java.lang.String>(org.seasar.doma.wrapper.StringWrapper::new, false));
            java.lang.String __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "selectUserName", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "selectUserName", __e);
            throw __e;
        }
    }

    @Override
    public java.lang.String selectUserPass(int userId) {
        entering("DB.UserDaoImpl", "selectUserPass", userId);
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method6);
            __query.setMethod(__method6);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/DB/UserDao/selectUserPass.sql");
            __query.addParameter("userId", int.class, userId);
            __query.setCallerClassName("DB.UserDaoImpl");
            __query.setCallerMethodName("selectUserPass");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.lang.String> __command = getCommandImplementors().createSelectCommand(__method6, __query, new org.seasar.doma.internal.jdbc.command.BasicSingleResultHandler<java.lang.String>(org.seasar.doma.wrapper.StringWrapper::new, false));
            java.lang.String __result = __command.execute();
            __query.complete();
            exiting("DB.UserDaoImpl", "selectUserPass", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("DB.UserDaoImpl", "selectUserPass", __e);
            throw __e;
        }
    }

}
