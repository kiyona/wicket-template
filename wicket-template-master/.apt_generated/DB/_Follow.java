package DB;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-07-29T17:09:32.388+0900")
public final class _Follow extends org.seasar.doma.jdbc.entity.AbstractEntityType<DB.Follow> {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final _Follow __singleton = new _Follow();

    private final org.seasar.doma.jdbc.entity.NamingType __namingType = org.seasar.doma.jdbc.entity.NamingType.SNAKE_LOWER_CASE;

    /** the followid */
    public final org.seasar.doma.jdbc.entity.AssignedIdPropertyType<java.lang.Object, DB.Follow, java.lang.Integer, Object> $followid = new org.seasar.doma.jdbc.entity.AssignedIdPropertyType<>(DB.Follow.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "followid", "", __namingType, false);

    /** the userid */
    public final org.seasar.doma.jdbc.entity.AssignedIdPropertyType<java.lang.Object, DB.Follow, java.lang.Integer, Object> $userid = new org.seasar.doma.jdbc.entity.AssignedIdPropertyType<>(DB.Follow.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "userid", "", __namingType, false);

    private final java.util.function.Supplier<org.seasar.doma.jdbc.entity.NullEntityListener<DB.Follow>> __listenerSupplier;

    private final boolean __immutable;

    private final String __catalogName;

    private final String __schemaName;

    private final String __tableName;

    private final boolean __isQuoteRequired;

    private final String __name;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> __idPropertyTypes;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> __entityPropertyTypes;

    private final java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> __entityPropertyTypeMap;

    private _Follow() {
        __listenerSupplier = () -> ListenerHolder.listener;
        __immutable = false;
        __name = "Follow";
        __catalogName = "";
        __schemaName = "";
        __tableName = "follow";
        __isQuoteRequired = false;
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> __idList = new java.util.ArrayList<>();
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> __list = new java.util.ArrayList<>(2);
        java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> __map = new java.util.HashMap<>(2);
        __idList.add($followid);
        __list.add($followid);
        __map.put("followid", $followid);
        __idList.add($userid);
        __list.add($userid);
        __map.put("userid", $userid);
        __idPropertyTypes = java.util.Collections.unmodifiableList(__idList);
        __entityPropertyTypes = java.util.Collections.unmodifiableList(__list);
        __entityPropertyTypeMap = java.util.Collections.unmodifiableMap(__map);
    }

    @Override
    public org.seasar.doma.jdbc.entity.NamingType getNamingType() {
        return __namingType;
    }

    @Override
    public boolean isImmutable() {
        return __immutable;
    }

    @Override
    public String getName() {
        return __name;
    }

    @Override
    public String getCatalogName() {
        return __catalogName;
    }

    @Override
    public String getSchemaName() {
        return __schemaName;
    }

    @Override
    public String getTableName() {
        return getTableName(org.seasar.doma.jdbc.Naming.DEFAULT::apply);
    }

    @Override
    public String getTableName(java.util.function.BiFunction<org.seasar.doma.jdbc.entity.NamingType, String, String> namingFunction) {
        if (__tableName.isEmpty()) {
            return namingFunction.apply(__namingType, __name);
        }
        return __tableName;
    }

    @Override
    public boolean isQuoteRequired() {
        return __isQuoteRequired;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preInsert(DB.Follow entity, org.seasar.doma.jdbc.entity.PreInsertContext<DB.Follow> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preUpdate(DB.Follow entity, org.seasar.doma.jdbc.entity.PreUpdateContext<DB.Follow> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preDelete(DB.Follow entity, org.seasar.doma.jdbc.entity.PreDeleteContext<DB.Follow> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preDelete(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postInsert(DB.Follow entity, org.seasar.doma.jdbc.entity.PostInsertContext<DB.Follow> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postUpdate(DB.Follow entity, org.seasar.doma.jdbc.entity.PostUpdateContext<DB.Follow> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postDelete(DB.Follow entity, org.seasar.doma.jdbc.entity.PostDeleteContext<DB.Follow> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postDelete(entity, context);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> getEntityPropertyTypes() {
        return __entityPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?> getEntityPropertyType(String __name) {
        return __entityPropertyTypeMap.get(__name);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<DB.Follow, ?>> getIdPropertyTypes() {
        return __idPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.GeneratedIdPropertyType<java.lang.Object, DB.Follow, ?, ?> getGeneratedIdPropertyType() {
        return null;
    }

    @Override
    public org.seasar.doma.jdbc.entity.VersionPropertyType<java.lang.Object, DB.Follow, ?, ?> getVersionPropertyType() {
        return null;
    }

    @Override
    public DB.Follow newEntity(java.util.Map<String, org.seasar.doma.jdbc.entity.Property<DB.Follow, ?>> __args) {
        DB.Follow entity = new DB.Follow();
        __args.values().forEach(v -> v.save(entity));
        return entity;
    }

    @Override
    public Class<DB.Follow> getEntityClass() {
        return DB.Follow.class;
    }

    @Override
    public DB.Follow getOriginalStates(DB.Follow __entity) {
        return null;
    }

    @Override
    public void saveCurrentStates(DB.Follow __entity) {
    }

    /**
     * @return the singleton
     */
    public static _Follow getSingletonInternal() {
        return __singleton;
    }

    /**
     * @return the new instance
     */
    public static _Follow newInstance() {
        return new _Follow();
    }

    private static class ListenerHolder {
        private static org.seasar.doma.jdbc.entity.NullEntityListener<DB.Follow> listener = new org.seasar.doma.jdbc.entity.NullEntityListener<>();
    }

}
