```
git clone git@github.com:tyakashita/wicket-template.git
Import > Existing Maven Project
Maven > Update Project
```

## このプロジェクトを動かす上で学ぶべきこと

1. Apache Maven  
ビルドツール。自前のプログラムを動かす時に依存しているアーティファクトの必要なバージョンをダウンロードしてくれる。もちろんダウンロードしたアーティファクトは使うことができる。
2. Apache Wicket  
WepページやHTMLの各コンポーネントをJavaのクラスで表現し、対応付けしたことによりJavaエンジニアにも馴染みやすくしたフレームワーク
3. Google Guice  
DI (Dependency Injection)フレームワーク。依存性の介入と日本語で表現する。
4. Doma2  
DBのORマッパーフレームワーク。似た製品がいろいろあるなか日本製。
